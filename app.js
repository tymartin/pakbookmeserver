const express = require('express');
const bodyParser = require('body-parser'); 
const mongoose = require('mongoose');
const cors = require('cors'); 
const passport = require('passport');

// initialize server
const app = express();
const port = process.env.PORT || 8000 

// middleware
app.use(bodyParser.json())

// localhost
// mongoose.connect('mongodb://localhost/bookingsystem', () => {
// console.log('Connected to database')
// });

// onlinehost
mongoose.connect('mongodb+srv://admin:test123@cluster0-qyycl.mongodb.net/test?retryWrites=true&w=majority', () => {
console.log('Connected to database')
});

app.use(passport.initialize())
app.use(cors())
app.use(bodyParser.json()); 

app.use('/users', require('./routes/users'));
app.use('/categories', require('./routes/categories')); 
app.use('/advisers', require('./routes/advisers')); 

    

app.listen( port, () => {
console.log(`Listening in port ${port}`)
})
