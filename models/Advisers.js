const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const AdviserSchema = new Schema({
	name : {
		type : String,
		required : [true, "Adviser name is required"]
	},

	categoryId : {

		type : String,
		required : [true, "Category ID is required"]
	},

	price : {
		type : Number,
		required : [true, "Adviser Fee is required"]
	},
     
	description : {
		type : String,
		required :    [true, "Adviser description is required"]
	},

	image : {
		type : String,
		required : [true, "Adviser image is required"]
	}
});

module.exports = mongoose.model('Adviser', AdviserSchema);
