const router = require('express').Router();
const AdviserModel = require('../models/Advisers');
const multer = require('multer');
const passport = require('passport')

function isAdmin(req,res,next){
	if(req.user.role !== 'admin') {
		return res.send({ message : "Unauthorized request"})
	} else {
		next ()
	}
}

const storage = multer.diskStorage({
	destination : function(req,file,cb){
		cb(null, "public/advisers")
	},
	filename : function(req,file,cb){
		cb(null, file.originalname)
	}
})

const upload = multer({ storage : storage });




router.get('/', function(req, res, next) {

	AdviserModel.find()
	.then( adviser => {
		res.json(adviser)
	})
	.catch(next);
});

// single
router.get('/:id', (req, res, next) => {
	
	AdviserModel.findOne({ _id : req.params.id})
	.then( adviser => res.json(adviser) )
	.catch(next)
});
  
// create
router.post('/', passport.authenticate('jwt', { session: false}),
isAdmin, upload.single('image'),(req,res, next) => {
	
	// res.json(req.file)
	req.body.image = "/public/" + req.file.filename;
	AdviserModel.create(req.body)
	.then( (adviser)=> res.json(adviser))
	.catch(next);
});

// update
router.put('/:id', 
	passport.authenticate('jwt', { session: false}),
	isAdmin, 
	upload.single('image'), 
	(req,res,next) => {
		let update = {
			...req.body
		}

		if(req.file){
			update = {
				...req.body,
				image: "/public/" + req.file.filename
			}
		}
	
	AdviserModel.findByIdAndUpdate(req.params.id, update, {new:true})
	.then(adviser => res.json(adviser))
	.catch(next);
	}
)

router.delete('/:id',(req,res,next) => {
	AdviserModel.findOneAndDelete({ _id : req.params.id })
	.then( adviser => res.json(adviser) )
	.catch(next)
})

module.exports = router;
