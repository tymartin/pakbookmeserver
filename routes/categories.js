const express = require('express'); 
const router = express.Router(); 
const passport = require('passport'); 

const CategoryModel = require('../models/Categories'); 

//Index
router.get('/', function(req,res,next){

    CategoryModel.find()
    .then( categories => {
        res.json(categories)
    })
    .catch(next); 
});

//Single 
router.get('/:id', (req,res, next) => {
    CategoryModel.findOne({ _id : req.params.id})
    .then( category => res.json(category))
    .catch(next); 
});
     
//create 
router.post('/', passport.authenticate('jwt', {session : false}),(req,res,next) => {

    if (req.user.role !== 'admin') {
        return res.send({message : "Unauthorized request"})
    }
    
    CategoryModel.create(req.body)
    .then( (category)=> {
        res.send(category)
        })
        .catch(next)
    }); 
    
// update
    router.put('/:id',(req,res,next) => {
        CategoryModel.findOneAndUpdate(
            {
                _id : req.params.id
            },
            {
                name : req.body.name
            },
            {
                new : true,
            }
        )
        .then( category => res.json(category) )
        .catch(next)
    })
    
    
    router.delete('/:id',(req,res,next) => {
        CategoryModel.findOneAndDelete({ _id : req.params.id })
        .then( category => res.json(category) )
        .catch(next)
    })
    
    module.exports = router;