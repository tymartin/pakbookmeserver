const express = require('express');
const router = express.Router();
const Adviser = require('./../models/Advisers');
const Transaction = require('./../models/Transactions');
const User = require('./../models/Users');
const passport = require('passport');
const auth = require('./../auth');

router.get('/', passport.authenticate('jwt', {session : false}), (req,res,next)=> {
if(req.user.role==="admin"){
	Transaction.find().then(transactions=> {
		User.find().then(users => {
		transactions.map( transaction => {
			users.find(user => {
				let matchedUser = user.find(user => {
				return 
					transaction.userId == user._id
				})

				transaction.username = matchedUser.firstname + "" + matchedUser.lastname; 
				return transaction;  
			})
		})
		res.send(transactions)			
		})
	})
}else {
	Transaction.find({userId: req.user.id})
	.then(transactions=> {
		User.find()
		.then(users => {
			transactions.map( transaction => {
				users.forEach(user => {
					if(transaction.userId == user._id){
						transaction.userId = user.firstname + "" + user.lastname; 
					}
					})
					return 
				}); 
				res.send(transactions)
			})
		})
}
})

router.post('/orders', (req,res,next) => {
	let orders = req.body.orders;

	let orderIds = orders.map( product => {
		return product.id
	} )



	// res.send(req.body)
	Adviser.find({ _id : orderIds })
	.then( products => {
		let total = 0;

		let newAdvisers = advisers.map( adviser => {
			let matchedAdviser ={};
			orders.forEach( order => {
				if (adviser.id === order.id) {

					matchedAdviser = {
						_id : adviser._id,
						name : adviser.name,
						price : adviser.price,
						image : adviser.image,
						
					}
				
				}
			})
			total += matchedAdviser.subtotal
			return matchedAdviser;
		})
		res.send({
			advisers : newAdvisers,
			total
		})
	})
})

router.post('/', passport.authenticate('jwt', {session : false}) ,(req,res,next) => {
	// userId : req.user._id
	// transactionCode : Date.now + 
	// total,
	// produts
	let orders = req.body.orders;

	let orderIds = orders.map( adviser => {
		return adviser.id
	} )



	// res.send(req.body)
	Adviser.find({ _id : orderIds })
	.then( advisers => {
		let total = 0;

		let newAdvisers = advisers.map( adviser => {
			let matchedAdviser ={};
			orders.forEach( order => {
				if (adviser.id === order.id) {

					matchedAdviser = {
						adviserId : adviser._id,
						name : adviser.name,
						price : adviser.price,
						image : adviser.image,
						subtotal : order.qty * adviser.price
					}
				
				}
			})
			total += matchedAdviser.subtotal;
			return matchedAdviser;
		})

		let transaction = {
			userId : req.user._id,
			transactionCode : Date.now(),
			total,
			advisers : newAdvisers
		}
		
		// return res.send(transaction)
		Transaction.create(transaction)
		.then(transaction => {
			return res.send(transaction)
		})

	})

}) 

//start of stripe 

router.post('/stripe',(req,res,next) => {

	let total = req.body.total;
	
	User.findOne({_id : req.body.userId})
	.then( user => {
		if(!user) {
			res.status(500).send({message: 'Incomplete'})
		} else {
			if(!user.stripeCustomerId){
	            stripe.customers
	            .create({
	                email: user.email,
	            })
	            .then(customer => {
	                return User.findByIdAndUpdate({ _id: user.id},{stripeCustomerId : customer.id}, {new:true})
	            })
	            .then( user => {
	                return stripe.customers.retrieve(user.stripeCustomerId)
	            })
	            .then((customer) => {
	                return stripe.customers.createSource(customer.id, {
	                source: 'tok_visa',
	                });
	            })
	            .then((source) => {
	                return stripe.charges.create({
	                amount: total,
	                currency: 'usd',
	                customer: source.customer,
	                });
	            })
	            .then((charge) => {
	                // New charge created on a new customer
	                console.log(charge)

	                res.send(charge);
	            })
	            .catch((err) => {
	                // Deal with an error
	                res.send(err)
	            });
	        } else {
                stripe.customers.retrieve(user.stripeCustomerId)
                .then((customer) => {
                    return stripe.customers.createSource(customer.id, {
                    source: 'tok_visa',
                    });
                })
                .then((source) => {
                    return stripe.charges.create({
                    amount: total * 100, //multiply by 100 for centavos
                    currency: 'usd',
                    customer: source.customer,
                    });
                })
                .then((charge) => {
                    // New charge created on a new customer
                    // console.log(charge)
                    res.send(charge);
                })
                .catch((err) => {
                    // Deal with an error
                    res.send(err)
                });
            }


		}
	})

})

router.put('/:id',passport.authenticate('jwt', {session : false}), auth,(req,res,next) =>{
	Transaction.findByIdAndUpdate(req.params.id,{ status : req.body.status}, { new : true})
	.then( transaction => res.send(transaction))
} ) 
module.exports = router;
